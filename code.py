import unittest

def fun(x):
    if x == 5:
	return "WOW"
    if x == 1999:
	y = []
	for x in range(5,10):
	    y.append(x)
	    x += 6
    return x + 1

def new_fun():
    y = 6
    if y == 6:
	k = 1
    else:
        k = 10
    return k

class UnitTests(unittest.TestCase):
    def test(self):
        self.assertEqual(fun(3), 4)
    def test_new_func_returns_1(self):
	self.assertEqual(new_fun(), 1)

class FeatureTests(unittest.TestCase):
    def test_5_is_wow(self):
	self.assertEqual(fun(5), "WOW")
